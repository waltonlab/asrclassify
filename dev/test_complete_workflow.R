usethis::proj_activate(usethis::proj_get())
devtools::load_all()

# CHANGE this path to your data
path <- 'inst/extdata/example_waveforms_generic_cols.csv'
wfms <- load_waveforms_generic_cols(path, time_ses=0.1, time_unit='msec')

# see several examples
plot(wfms[,1:10])

# feature engineering - extract features
# below demonstrates how to customize the continuous wavelet transform
# features of interest
cwt.fois <- cwt_fois(list(
    cwt_foi('mean.cwt.before.startle', t=c(-Inf,0))
  , cwt_foi('mean.cwt.before.startle.to.max.power', t=c(0, 't.max.power'))
  , cwt_foi('mean.cwt')
  , cwt_foi('mean.cwt.low.freq', period=2^-4.5)
  , cwt_foi('mean.cwt.high.freq', period=c(2^-8, 2^-7))
  , cwt_foi('mean.cwt.around.startle', t=-0.02, delt=0.02)
  , cwt_foi('mean.cwt.well.before.startle', t=-0.09, delt=0.01)
  , cwt_foi('mean.cwt.during.startle', t=0.01, delt=0.01)
  , cwt_foi('mean.cwt.after.startle', t=0.09, delt=0.1)
  , cwt_foi('max.cwt.after.startle', t=c(0, Inf), fun='max')
))
fois <- combine_fois(wfm.fois=default_wfm_fois(norm.method='before'),
                     psd.fois=default_psd_fois(norm.method='before'),
                     cwt.fois=cwt.fois)

# extract feats
feats <- extract_feats(wfms, fois=fois)
classes <- factor(ifelse(colnames(wfms) == '1', 'startle', 'nonstartle'))
feats$startle <- classes
# see the features
feats

# train model
# partition the data
feats_split <- partition_data(feats)
# train default models (right now, only rf via ranger and svm)
model_st <- train_models(feats_split)

# see details of the stacking
# requires ggplot2
# library(ggplot2)
# autoplot(model_st)
# autoplot(model_st, type='weights')

# evaluate performance
evaluate_performance(model_st, feats_split=feats_split)
