% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/load_waveforms.R
\name{load_waveforms_mat}
\alias{load_waveforms_mat}
\title{Load startle waveforms stored in a Matlab StartleData structure}
\usage{
load_waveforms_mat(
  path,
  var = "StartleData",
  time_unit = "msec",
  load_filtered = FALSE
)
}
\arguments{
\item{path}{path to .mat file}

\item{var}{variable within the .mat file the StartleData structure is in}

\item{time_unit}{character string of time units c('s', 'sec', 'ms', 'msec')}
}
\value{
ts object
}
\description{
Load startle waveforms stored in a Matlab StartleData structure
}
\examples{
add(1, 1)
add(10, 1)
}
