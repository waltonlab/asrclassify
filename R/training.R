# data partitioning

#' @export
partition_data <- function(feats, prop=0.8, seed=NULL, ...) {
  set.seed(seed)
  rsample::initial_split(feats, prop=prop, ...)
}

#' @export
create_vfold_cv <- function(feats_split, seed=NULL, v=5, repeats=2, ...) {
  # only if v not null
  if (!is.null(v)) {
    set.seed(seed)
    feats_split %>%
      rsample::training() %>% 
      rsample::vfold_cv(v=v, repeats=repeats, ...)
  } else {
    rsample::manual_rset(
      list(rsample::make_splits(training(feats_split),
                           assessment=testing(feats_split))),
      ids=c('Complete'))
  }
}


      




#' @export
default_vfold_cv <- function(feats_split, seed=NULL, v=5, repeats=2) {

}

# defaults


# DB - in paper - add another example of a custom recipe (i.e. pca versus
# normalize)
#' @export
default_recipe <- function(feats_split) {
  feats_split %>%
    rsample::training() %>%
    recipes::recipe(startle ~ .) %>%
    recipes::step_nzv(recipes::all_predictors()) %>%
    recipes::step_corr(recipes::all_predictors()) %>%
    recipes::step_lincomb(recipes::all_predictors()) %>%
    recipes::step_normalize(recipes::all_predictors(), -recipes::all_outcomes()) %>%
    recipes::prep()
}


# DB - in paper - limit number of models (rf only as an example) - for faster
# initial training purposes - train FULL model when done with feature
# engineering and initial model development

#' @export
default_specs <- function() {
  list(rf=rf_spec(), svm=svm_spec())
}

#' @export
default_grid <- function() {
  10
}

# model specs

#' @export
rf_spec <- function(engine='ranger', ...) {
  parsnip::rand_forest(mtry=tune::tune(), trees=tune::tune(), min_n=tune::tune()) %>%
    parsnip::set_engine(engine, ...) %>%
    parsnip::set_mode('classification')
}

#' @export
svm_spec <- function(engine='kernlab', ...) {
  parsnip::svm_rbf(cost = tune::tune(), rbf_sigma = tune::tune('sigma')) %>%
    parsnip::set_engine(engine, ...) %>%
    parsnip::set_mode('classification')
}

# utils

#' @export
create_workflow <- function(spec, rec) {
  workflows::workflow() %>%
    workflows::add_model(spec) %>% 
    workflows::add_recipe(rec)
}

need_tune <- function(x) {
  nrow(tune::tune_args(x)) > 0
}

# fit a single model

#' @export
train_model <- function(spec, rec, folds, metrics, ctrl_grid, ctrl_res, grid, ...) {
    # create the workflow
    wflow <- create_workflow(spec, rec)
    # train (including tuning if needed)
    if (need_tune(wflow)) {
      tune::tune_grid(
        wflow,
        resamples = folds,
        metrics = metrics,
        grid = grid,
        control = ctrl_grid
      )
    } else {
      tune::fit_resamples(
        wflow,
        resamples = folds,
        metrics = metrics,
        control = ctrl_res
      )
    }
}

# train multiple models, then stack them

#' @export
train_models <- function(feats_split,
                       recipe.FUN=default_recipe,
                       specs=default_specs(), 
                       metrics=default_metrics(),
                       ctrl_grid=stacks::control_stack_grid(),
                       ctrl_res=stacks::control_stack_resamples(), 
                       grid=default_grid(),
                       ...) {
  folds <- create_vfold_cv(feats_split, ...)
  feats_rec <- feats_split %>% 
    recipe.FUN()
  # train the individual models - including parameter tuning if defined
  cat('Training the individual models:\n')
  models <- lapply(specs, train_model, rec=feats_rec, folds=folds, 
                   metrics=metrics, ctrl_grid=ctrl_grid, ctrl_res=ctrl_res, 
                   grid=grid)
  # create the stacked model
  st <- stacks::stacks()
  for (i in seq_along(models)) {
    st <- st %>% stacks::add_candidates(models[[i]], name=names(models)[i])
  }
  # train the stack
  cat('Training the stacked model:\n')
  st %>% stacks::blend_predictions() %>% stacks::fit_members()
}
