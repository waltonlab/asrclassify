# asrclassify: Explore, Extract Features From, and Classify Acoustic Startle Reflex Waveforms as Startles and Non-Startles

[Fawcett, T. J., Longenecker, R. J., Brunelle, D. L., Berger, J. I., Wallace,
M. N., Galazyuk, A. V., Rosen, M. J., Salvi, R. J., & Walton, J. P. (2023).
Universal automated classification of the acoustic startle reflex using
machine learning. Hearing Research, 428,
108667.](https://doi.org/10.1016/J.HEARES.2022.108667)

<!-- badges: start -->
<!-- badges: end -->

The goal of asrclassify is to classify acoustic startle reflex (ASR) waveforms
as startles versus non-startles through the full data science process of data
exploration, feature extraction, and classification via machine learning.

## Current Test/Installation

Since this package is currently in development, please use the following
instructions to test/install this package.

``` bash
# clone the repo
git clone https://gitlab.com/waltonlab/asrclassify.git
# change to the dev dir
cd asrclassify
#launch R and run/source the dev/test_complete_workflow.R script
```
## Filtering

At the time of this writing, filtering is the responsibility of the user and
is currently provided by this package.

## Required packages

The `devtools::load_all()` in
[dev/test_complete_workflow.R](dev/test_complete_workflow.R) should install
all required packages.

<!-- Since this package has not yet been deployed, please ensure you have current -->
<!-- versions of the following packages installed to test/use this package.  NOTE: -->
<!-- this list may not be exhaustive. -->

<!-- 1. data.table -->
<!-- 1. dplR -->
<!-- 1. biwavelet -->
<!-- 1. WaveletComp -->
<!-- 1. cubelyr -->
<!-- 1. yardstick -->
<!-- 1. signal -->
<!-- 1. R.matlab -->
<!-- 1. rsample -->
<!-- 1. recipes -->
<!-- 1. parsnip -->
<!-- 1. workflows -->
<!-- 1. tune -->
<!-- 1. stacks -->
<!-- 1. dplyr -->

<!-- [> ### Complete detailed documentation <] -->

<!-- Complete documentation of every function including real examples -->

<!-- ### Planned vignettes -->

<!-- 1. Complete default workflow - similar to dev/test_complete_workflow.R but -->
<!--    with ONLY default features, model, etc... choices -->
<!-- 1. Custom cwt features - similar to dev/test_complete_workflow.R but -->
<!--    with computation and plotting cwts to determine best cwt features of -->
<!--    interest (same as those used in test_complete_workflow.R) -->
<!-- 1. Custom recipe - how to customize the recipe -->
<!-- 1. Custom spec - host to customize the model specifications (i.e. add a new -->
<!--    model to the workflow) -->

<!-- ## Future (i.e. NOT YET) Installation -->

<!-- You can install the released version of asrclassify from [CRAN](https://CRAN.R-project.org) with: -->

<!-- ``` r -->
<!-- install.packages("asrclassify") -->
<!-- ``` -->

<!-- ## Future Example -->

<!-- This is a basic example which shows you how to solve a common problem: -->

<!-- ``` r -->
<!-- library(asrclassify) -->
<!-- ## basic example code -->
<!-- ``` -->

